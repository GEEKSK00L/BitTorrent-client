# BitTorrent-client
Bit torrent client implementing the *BitTorrent protocol* in Python using libtorrent an open source C++ library

## Requirements

 + libtorrent-rasterbar 
 

## Usage

``` bash

$ git clone https://github.com/R3DDY97/BitTorrent-client

cd  BitTorrent-client

python client.py <torrentfile> or <magnet-url>

```
